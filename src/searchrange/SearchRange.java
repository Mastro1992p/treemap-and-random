/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package searchrange;

import java.util.NavigableMap;
import java.util.Random;
import java.util.TreeMap;

/**
 *
 * @author Mastro
 */
public class SearchRange {

    public double min = 0d;
    public double max = 100d;
    public NavigableMap <Double,Node> map;
    
    public SearchRange(){
        this.map = new  TreeMap();
    }
    
    public void defaultRanges(){
        
        this.map.put(0d, new Node("A",0,0d));
        this.map.put(33.33d, new Node("B",0,33.33d));
        this.map.put(80.25d, new Node("C",0,80.25d));
        
    }
    
    public Node getGroup(double Key){
        
        if(Key < 0 || Key >100){
            System.out.println("Fuera de rango");
            return null;
        }else{
           Node aux = map.floorEntry(Key).getValue();
           return aux;
        }
    }
    
    public void UpdateNode(Node aux){
        
        this.map.put(aux.rangeNum, aux);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        SearchRange sr = new SearchRange();
        sr.defaultRanges();
       
        Random r = new Random(System.currentTimeMillis());
        
        
        for(int i = 0; i < 10000000; i ++){
            
            double randomV = sr.min + (sr.max-sr.min) * r.nextDouble();
            Node aux = sr.getGroup(randomV);
            aux.AumentCont();
            sr.UpdateNode(aux);
            
        }
        
        for(Node n : sr.map.values()){
            
            System.out.println("Values in "+n.name+" cont "+n.cont+" Prob "+n.rangeNum);
        }
        
    }
    
}
