/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package searchrange;


/**
 *
 * @author Mastro
 */
public class Node {
    
    public String name;
    public int cont;
    public double rangeNum;

    public Node(String name, int cont, double rangeNum) {
        this.name = name;
        this.cont = cont;
        this.rangeNum = rangeNum;
    }
    
    public Node(){
        
    }

    public String getName() {
        return name;
    }

    public int getCont() {
        return cont;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCont(int cont) {
        this.cont = cont;
    }

    public double getRangeNum() {
        return rangeNum;
    }

    public void setRangeNum(double rangeNum) {
        this.rangeNum = rangeNum;
    }
       
    public void AumentCont(){
        this.cont ++;
    }
}
